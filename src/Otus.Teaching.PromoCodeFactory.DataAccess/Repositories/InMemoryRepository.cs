﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<IEnumerable<T>> DeleteByIdAsync(Guid id)
        {
            IEnumerable<T> res = new List<T>();

            var item = Data.FirstOrDefault(x => x.Id == id);

            if (item != null)
            {
                Data = Data.Where(x => x.Id != id);
                res = Data;
            }
            return Task.FromResult(res);
        }

        public Task<IEnumerable<T>> CreateAsync(T newItem)
        {
            IEnumerable<T> res = new List<T>();

            var item = Data.FirstOrDefault(x => x.Id == newItem.Id);

            if (item == null)
            {
                List<T> dt = Data.ToList();
                dt.Add(newItem);
                Data = dt;
                res = Data;
            }

            return Task.FromResult(res);
        }

        public Task<IEnumerable<T>> UpdateAsync(T updatedItem)
        {
            IEnumerable<T> res = new List<T>();
            var item = Data.FirstOrDefault(x => x.Id == updatedItem.Id);

            if (item != null)
            {
                List<T> dt = Data.ToList();
                dt.Remove(item);
                dt.Add(updatedItem);
                Data = dt;
                res = Data;
            }

            return Task.FromResult(res);
        }

    }
}